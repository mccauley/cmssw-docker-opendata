#!/bin/bash

start_vnc() {
    nvnc=$((`vncserver -list | wc -l`-4))
    vncname="myvnc:$(($nvnc+1))"
    desktop="`hostname`:$(($nvnc+1))"
    vncserver -geometry $GEOMETRY -name $vncname -PasswordFile=$HOME/.vnc/passwd
    export ORIGINAL_DISPLAY=$DISPLAY
    export DISPLAY=$desktop
    /usr/local/novnc/utils/novnc_proxy --vnc localhost:5901 > $HOME/.vnc/novnc_proxy.log 2>&1 &
    echo -e "VNC connection points:"
    echo -e "\tVNC viewer address: 127.0.0.1:$((5900+$nvnc+1))"
    echo -e "\tHTTP access: http://127.0.0.1:6080/vnc.html"
    echo -e "To kill the vncserver enter 'vncserver -kill :$(($nvnc+1))'"
}

stop_vnc() {
    nvnc=$((`vncserver -list | wc -l`-4))
    for i in $(seq 1 $nvnc); do
        vncserver -kill :${i}
    done
    export DISPLAY=$ORIGINAL_DISPLAY
}

clean_vnc() {
    stop_vnc
}